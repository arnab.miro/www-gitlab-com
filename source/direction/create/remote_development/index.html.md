---
layout: markdown_page
title: "Direction - Remote Development"
description: This is the direction page for Remote Development in GitLab.
canonical_path: "/direction/create/remote_development/"
---

Last reviewed: 2021-11-17

- TOC
{:toc}

## Remote Development

### Introduction and how you can help

This is the direction page for Remote Development in GitLab. It is not officially a part of any DevOps stage nor is it an official marketing category. Eric Schurter ([@ericschurter](https://gitlab.com/ericschurter), [eschurter@gitlab.com](mailto:eschurter@gitlab.com)) Senior Product Manager for the [Editor group](/direction/create/editor/), maintains this page and the vision, as it relates closely to the [Web IDE](/direction/create/editor/web_ide/) and related features.

Like most other category direction pages, this page will be continuously updated based on market dynamics, new data points, and customer conversations. Please share feedback directly via email, Twitter, or on a video call.

### Overview

More information about this emerging category will be added in the near future.

<!-- A good description of what your category is today or in the near term. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Provide enough context that someone unfamiliar
with the details of the category can understand what is being discussed. -->

<!-- #### Introduction -->

<!-- - Web IDE current state
- Remote execution of code 
- Web or local IDE
- Relationship to Runners -->

<!-- #### What is Remote Development? -->


<!-- #### Target Audience -->



<!-- #### What's Next & Why -->

### Related issues

- [IDE / editor with a local editor via mirroring / sync](https://gitlab.com/gitlab-org/gitlab/-/issues/16069)
- [Remote Development Environments](https://gitlab.com/groups/gitlab-org/-/epics/3230)
- [Server Runtime](https://gitlab.com/gitlab-org/gitlab/-/issues/329602)


### Opportunity reviews

- [Previous opportunity canvas](https://docs.google.com/document/d/1t1j98Wl1erG9b8cUT77yDsNUEPvCOMOp0ktbOkYZfWc/edit#heading=h.4mt5fmtn0ax4)
- [Investigation into replacing Web IDE with Theia](https://gitlab.com/groups/gitlab-org/-/epics/1619)

### Competitive Landscape

<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/product-processes/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

- GitHub Codespaces
- Coder
- Gitpod
- Theia
- Eclipse Che
- CodeSandbox
- Cloud9 IDE
- Codeanywhere
- JetBrains


<!-- ### Analyst Landscape -->

<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

